package com.spotty.models

data class Clip(
    val id: Int,
    val userId: Int,
    val name: String,
    val description:String,
    val srcPath: String
)