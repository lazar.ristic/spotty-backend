package com.spotty.repository

import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.Column

object Clips : IntIdTable() {
    val userId: Column<Int> = integer("userId").references(Users.id)
    val todo = varchar("todo", 512)
    val done = bool("done")
    val description = varchar("todo", 512)
    val srcPath = varchar("todo", 256)
}